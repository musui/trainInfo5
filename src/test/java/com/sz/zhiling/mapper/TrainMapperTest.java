package com.sz.zhiling.mapper;

import com.sz.zhiling.model.TrainInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @Author 杜微
 * @Date 2021/7/22 10:36
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TrainMapperTest {

    @Autowired
    TrainInfoMapper mapper;

    @Test
    public void test() {
        List<TrainInfo> trainInfos = mapper.selectAll();
        System.out.println("trainInfos = " + trainInfos);
    }

}
