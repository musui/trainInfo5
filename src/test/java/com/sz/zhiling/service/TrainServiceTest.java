package com.sz.zhiling.service;

import com.github.pagehelper.PageInfo;
import com.sz.zhiling.model.TrainInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Author 杜微
 * @Date 2021/7/22 11:52
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TrainServiceTest {

    @Autowired
    ITrainService trainService;

    /**
     * 分页查询
     */
    @Test
    public void test() {
        PageInfo<TrainInfo> trainInfoPageInfo = trainService.showAllTrain(1, 3);
        List<TrainInfo> list = trainInfoPageInfo.getList();
        for (TrainInfo t : list) {
            System.out.println("t = " + t);
        }
        System.out.println("trainInfoPageInfo = " + trainInfoPageInfo);
    }

    @Test
    public void test2() {
        TrainInfo trainInfo = trainService.showTrain("D14");
        System.out.println("trainInfo = " + trainInfo);
    }
}
