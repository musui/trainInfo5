package com.sz.zhiling.controller;

import com.github.pagehelper.PageInfo;
import com.sz.zhiling.model.Result;
import com.sz.zhiling.model.TrainInfo;
import com.sz.zhiling.service.ITrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author 杜微
 * @Date 2021/7/22 11:58
 * @Version 1.0
 */
@CrossOrigin
@RestController
public class TrainController {

    @Autowired
    ITrainService service;

    /**
     *
     * @param index
     * @param pageSize
     * @return
     * 分页显示所有车次
     */
    @RequestMapping("/show")
    public Result showAllTrain(@RequestParam(defaultValue = "1") int index, @RequestParam(defaultValue = "3") int pageSize) {
        PageInfo<TrainInfo> trainInfoPageInfo = service.showAllTrain(index, pageSize);
        return Result.getResult(200, "成功", trainInfoPageInfo);
    }

    /**
     *
     * @param id
     * @return
     * 显示该列车详细信息
     */
    @RequestMapping("/showDetail")
    public Result showTrain(String id) {
        TrainInfo trainInfo = service.showTrain(id);
        return Result.getResult(200, "查看当前详细成功", trainInfo);
    }

    /**
     * 删除列车信息
     * @param chooses
     * @return
     */
    @RequestMapping("/deleteTrain")
    public Result deleteTrain(@RequestBody List<String> chooses) {
        Integer integer = service.deleteTrains(chooses);
        return Result.getResult(200, "批量删除成功", integer);
    }

}
