package com.sz.zhiling.service;

import com.github.pagehelper.PageInfo;
import com.sz.zhiling.model.MyPage;
import com.sz.zhiling.model.TrainInfo;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author 杜微
 * @Date 2021/7/22 10:50
 * @Version 1.0
 * 火车业务
 * 1.带分页显示所有车次
 * 2.查看当前车次详细信息
 * 3.批量删除车次
 */
public interface ITrainService {


    /**
     * @param index    页面下标
     * @param pageSize 页面大小
     * @return 分页返回所有的车次
     */
    PageInfo<TrainInfo> showAllTrain(int index, int pageSize);


    /**
     * @param id 获取ID
     * @return 返回该此次详细信息
     */
    TrainInfo showTrain(String id);

    /**
     * @param chooses 获取的主键集合
     * @return
     */
    Integer deleteTrains(List<String> chooses);


}
