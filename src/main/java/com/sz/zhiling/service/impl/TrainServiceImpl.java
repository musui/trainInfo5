package com.sz.zhiling.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sz.zhiling.mapper.TrainInfoMapper;
import com.sz.zhiling.model.MyPage;
import com.sz.zhiling.model.TrainInfo;
import com.sz.zhiling.service.ITrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author 杜微
 * @Date 2021/7/22 11:29
 * @Version 1.0
 */
@Service
public class TrainServiceImpl implements ITrainService {

    @Autowired
    TrainInfoMapper mapper;


    @Override
    public PageInfo<TrainInfo> showAllTrain(int index, int pageSize) {
        PageHelper.startPage(index, pageSize);
        List<TrainInfo> trainInfos = mapper.selectAll();
        return new PageInfo<TrainInfo>(trainInfos);
    }

    @Override
    public TrainInfo showTrain(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public Integer deleteTrains(List<String> chooses) {
        Example example = new Example(TrainInfo.class);
        Example.Criteria criteria = example.createCriteria();
        for (String s : chooses) {
            criteria.orEqualTo("trainNo", s);
        }
        return mapper.deleteByExample(example);
    }
}
