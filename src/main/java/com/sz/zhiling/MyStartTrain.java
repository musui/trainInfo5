package com.sz.zhiling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @Author 杜微
 * @Date 2021/7/22 10:39
 * @Version 1.0
 */
@MapperScan("com.sz.zhiling.mapper")
@SpringBootApplication
public class MyStartTrain {

    public static void main(String[] args) {
        SpringApplication.run(MyStartTrain.class);
    }
}
