package com.sz.zhiling.model;

/**
 * @Author 杜微
 * @Date 2021/7/22 11:11
 * @Version 1.0
 * 统一返回响应结果
 */
public class Result<T> {

    private Integer ResultCode;

    private String ResultMessage;

    private Object object;

    private Result(Integer resultCode) {
        ResultCode = resultCode;
    }

    private Result(Integer resultCode, String resultMessage) {
        ResultCode = resultCode;
        ResultMessage = resultMessage;
    }

    private Result(Integer resultCode, Object object) {
        ResultCode = resultCode;
        this.object = object;
    }

    private Result(Integer resultCode, String resultMessage, Object object) {
        ResultCode = resultCode;
        ResultMessage = resultMessage;
        this.object = object;
    }

    public static Result getResult(Integer resultCode, String resultMessage, Object object) {
        if (resultMessage == null) {
            if (object == null) {
                return new Result(resultCode);
            }
            return new Result(resultCode, object);
        } else {
            if (object == null) {
                return new Result(resultCode, resultMessage);
            }
            return new Result(resultCode, resultMessage,object);

        }
    }

    public Integer getResultCode() {
        return ResultCode;
    }

    public void setResultCode(Integer resultCode) {
        ResultCode = resultCode;
    }

    public String getResultMessage() {
        return ResultMessage;
    }

    public void setResultMessage(String resultMessage) {
        ResultMessage = resultMessage;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "Result{" +
                "ResultCode=" + ResultCode +
                ", ResultMessage='" + ResultMessage + '\'' +
                ", object=" + object +
                '}';
    }
}
