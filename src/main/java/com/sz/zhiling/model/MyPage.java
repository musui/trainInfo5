package com.sz.zhiling.model;

import java.util.List;

/**
 * @Author 杜微
 * @Date 2021/7/22 10:54
 * @Version 1.0
 * 分页实体类
 */
public class MyPage<T> {

    private int index;
    private int pageSize;
    private int pageCount;
    private long total;
    private List<T> row;

    public MyPage() {
    }

    public MyPage(int index, int pageSize, int pageCount, long total, List<T> row) {

        this.index = index;
        this.pageSize = pageSize;
        this.pageCount = pageCount;
        this.total = total;
        this.row = row;
    }

    public int getIndex() {

        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getRow() {
        return row;
    }

    public void setRow(List<T> row) {
        this.row = row;
    }

    @Override
    public String toString() {
        return "MyPage{" +
                "index=" + index +
                ", pageSize=" + pageSize +
                ", pageCount=" + pageCount +
                ", total=" + total +
                ", row=" + row +
                '}';
    }
}
