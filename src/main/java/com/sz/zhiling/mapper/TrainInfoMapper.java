package com.sz.zhiling.mapper;

import com.sz.zhiling.model.TrainInfo;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface TrainInfoMapper extends Mapper<TrainInfo> {

}